<?php
    function tukar_besar_kecil($string){
        //kode di sini
        $keluaran = "";
        $string_len = strlen($string);
        

        for($i = 0; $i < $string_len; $i++) {
            
            $cekhuruf = ctype_upper($string[$i]);
            if ($cekhuruf) {
                $huruf = strtolower($string[$i]);
            } else {
                $huruf = strtoupper($string[$i]);
            }
            
            $keluaran .= $huruf;
        }
        
        return $keluaran . "<br>";
    }

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>